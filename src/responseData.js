export const data = [
    {
        "lat": 52.339498,
        "lon": 4.841021,
        "uid": 3716,
        "aqi": "18",
        "station": {
            "name": "A10-west, Amsterdam",
            "time": "2021-10-28T17:00:00+02:00"
        }
    },
    {
        "lat": 52.381331,
        "lon": 4.845233,
        "uid": 2673,
        "aqi": "15",
        "station": {
            "name": "Einsteinweg, Amsterdam",
            "time": "2021-10-28T17:00:00+02:00"
        }
    },
    {
        "lat": 52.374786,
        "lon": 4.860319,
        "uid": 7934,
        "aqi": "15",
        "station": {
            "name": "Jan van Galenstraat, Amsterdam",
            "time": "2021-10-28T16:00:00+02:00"
        }
    },
    {
        "lat": 52.358039,
        "lon": 4.8997,
        "uid": 2675,
        "aqi": "21",
        "station": {
            "name": "Stadhouderskade, Amsterdam",
            "time": "2021-10-28T17:00:00+02:00"
        }
    },
    {
        "lat": 52.393972,
        "lon": 4.870157,
        "uid": 2671,
        "aqi": "111",
        "station": {
            "name": "Westerpark, Amsterdam",
            "time": "2021-10-28T17:00:00+02:00"
        }
    },
    {
        "lat": 52.389983,
        "lon": 4.887811,
        "uid": 2676,
        "aqi": "19",
        "station": {
            "name": "Van Diemenstraat, Amsterdam",
            "time": "2021-10-28T17:00:00+02:00"
        }
    },
    {
        "lat": 52.359714,
        "lon": 4.866208,
        "uid": 2670,
        "aqi": "18",
        "station": {
            "name": "Vondelpark, Amsterdam",
            "time": "2021-10-28T17:00:00+02:00"
        }
    },
    {
        "lat": 52.3702157,
        "lon": 4.8951679,
        "uid": 5771,
        "aqi": "18",
        "station": {
            "name": "Amsterdam",
            "time": "2021-10-28T16:00:00+02:00"
        }
    },
    {
        "lat": 52.42023,
        "lon": 4.83206,
        "uid": 2668,
        "aqi": "22",
        "station": {
            "name": "Hemkade, Amsterdam",
            "time": "2021-10-28T17:00:00+02:00"
        }
    }
]





