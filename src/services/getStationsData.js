import axios from "axios";

export const getStationsData = (url) => {
  return new Promise((resolve, reject) => {
    axios({
      method: "get",
      url: url,
      responseType: "json",
    })
      .then(
        (response) => {
          if (response.data.status === "error") {
            console.log();
            return reject(response.data.data);
          }
          if (response.data.status === "ok") {
            return resolve(response.data.data);
          }
        },
        (err) => reject("Server Not Found", err)
      )
      .catch(function (error) {
        if (error.response) {
          alert(error.response.data);
          alert(error.response.status);
          console.log(error.response.headers);
        } else if (error.request) {
          alert(error.request);
        } else {
          alert("Error", error.message);
        }
        alert(error.config);
      });
  });
};
