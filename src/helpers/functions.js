export const responseToGeoJson = (data) => {
  const featuresArr = data.map((el) => {
    return {
      type: "Feature",
      geometry: {
        type: "Point",
        coordinates: [el.lon, el.lat],
      },
      properties: {
        id: el.uid,
        aqi: el.aqi,
        stationName: el.station.name,
        time: el.station.time,
      },
    };
  });
  return {
    type: "FeatureCollection",
    features: featuresArr,
  };
};

export const calculateAirHealthLevel = (aqi) => {
  if (aqi > 0 && aqi <= 15) {
    return { index: 0, description: "Excellent", color:"#a2ef61ff" };
  } else if (aqi > 15 && aqi <= 30) {
    return { index: 1, description: "Good", color:"#ffec50ff" };
  } else if (aqi > 30 && aqi <= 55) {
    return { index: 2, description: "Moderate", color:"#ffb14cff" };
  } else if (aqi > 55 && aqi <= 110) {
    return { index: 3, description: "Unhealthy",color:"#ff6275ff" };
  } else if (aqi > 110) {
    return { index: 4, description: "Extremly Unhealthy",color:"#b36ebeff" };
  } else {
    return -1;
  }
};

export const sortResposneObj = (obj)=> {
  // console.log('sortResposneObj', obj)
  const sorted = obj.sort((a,b) => (a.station.name > b.station.name) ? 1 : ((b.station.name > a.station.name) ? -1 : 0))
  // console.log('sortResposneObjsorted', sorted)
  return sorted

}

// Between 0 and 15 ppm: Excellent
// - Between 15 and 30 ppm: Good
// - Between 30 and 55 ppm: Moderate
// - Between 55 and 110 ppm: Unhealthy
// - Above 110 ppm: Extremly Unhealthy
