import * as React from "react";

import ReactMapGL, { FlyToInterpolator } from "react-map-gl";
import Marker from "../Marker/Marker";

const Map = ({ items, flyToItem }) => {
  const [viewport, setViewport] = React.useState({
    latitude: 52.377956,
    longitude: 4.89707,
    zoom: 12,
  });

  React.useEffect(() => {
    if (flyToItem) {
      setViewport({
        latitude: flyToItem.geometry.coordinates[1],
        longitude: flyToItem.geometry.coordinates[0],
        zoom: 13,
      });
    }
  }, [flyToItem]);

  const markers = React.useMemo(
    () =>
      items.features.map((item) => (
        <Marker key={item.properties.id} item={item} />
      )),
    [items.features]
  );

  return (
    <>
      <ReactMapGL
        {...viewport}
        width="100%"
        height="100%"
        onViewportChange={(viewport) => setViewport(viewport)}
        mapboxApiAccessToken={process.env.REACT_APP_MAPBOX_TOKEN}
        scrollZoom={false}
        doubleClickZoom={false}
        transitionDuration={1000}
        transitionInterpolator={new FlyToInterpolator({ speed: 1, curve: 1 })}
      >
        {markers}
      </ReactMapGL>
    </>
  );
};

export default Map;
