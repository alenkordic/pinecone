import React from "react";
import { Popup as MapGlPopup } from "react-map-gl";

import { calculateAirHealthLevel } from "./../../../helpers/functions";

import classes from "./Popup.module.css";

const Popup = ({ stationData, showPopup }) => {
  const { name, aqi, time, pm25today, coordinates } = stationData;

  const { avg, max, min } = pm25today;

  const { description, color } = calculateAirHealthLevel(aqi);

  return (
    <>
      {showPopup && (
        <MapGlPopup
          latitude={coordinates[0]}
          longitude={coordinates[1]}
          closeButton={false}
          closeOnClick={false}
          anchor="top"
          className={classes.mapGlPopup}
        >
          <div className={classes.container}>
            <div className={classes.popupHeader}>
              <h4 classNaame={classes.name}>{name}</h4>
              <div className={classes.time}>{time}</div>
            </div>
            <div className={classes.content}>
              <div className={classes.aqi} style={{ backgroundColor: color }}>
                <h2>{aqi}</h2>
                <span>{description}</span>
              </div>
              <div className={classes.values}>
                <span>PM2.5 values today</span>
                <div>Max: {max}</div>
                <div>Min: {min}</div>
                <div>Avarage: {avg}</div>
              </div>
            </div>
          </div>
        </MapGlPopup>
      )}
    </>
  );
};

export default Popup;
