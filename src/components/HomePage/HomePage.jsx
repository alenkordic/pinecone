import React, { useState, useEffect } from "react";

import Map from "../Map/Map";
import StationsList from "../StationsList/StationsList";
import SidebarHeader from "../SidebarHeader/SidebarHeader";
import LoaderSpiner from "../LoaderSpiner/LoaderSpiner";

import { getStationsData } from "../../services/getStationsData";

import { responseToGeoJson, sortResposneObj } from "./../../helpers/functions";

import classes from "./HomePage.module.css";

const HomePage = () => {
  const [stationsData, setStationsData] = useState(undefined);
  const [flyToItem, setFlytoItem] = useState(null);
  const [refreshData, setRefreshData] = useState(false);
  const [updatedDate, setUpdatedDate] = useState(null);

  const loadData = () => {
    const latlng = "52.315195,4.786606,52.425873,5.057144";
    const url = `https://api.waqi.info/map/bounds/?latlng=${latlng}&token=${process.env.REACT_APP_AQICN_TOKEN}`;
    getStationsData(url).then(
      (response) => {
        setStationsData(responseToGeoJson(sortResposneObj(response)));
        setUpdatedDate(new Date());
      },
      (err) => {
        alert(err);
      }
    );
  };

  useEffect(loadData, []);

  useEffect(() => {
    let interval;
    if (refreshData) {
      console.log(stationsData);
      loadData();
      interval = setInterval(loadData, 10000);
    }
    return () => clearInterval(interval);
  }, [refreshData]);

  const onFlyToCoordsHandler = (id) => {
    const flyTo = stationsData?.features?.filter((e) => e.properties.id === id);
    setFlytoItem(flyTo[0]);
  };

  const onLoadDataHandler = () => {
    setRefreshData((prevState) => {
      return !prevState;
    });
  };


  if (!stationsData) {
    return <LoaderSpiner />
  }

  return (
    <div className={classes.mainContainer}>
      <div className={classes.listContainer}>
        <SidebarHeader
          onLoadData={onLoadDataHandler}
          updatedDate={updatedDate}
          refreshData={refreshData}
        />
        {Object.keys(stationsData).length !== 0 && (
          <StationsList
            items={stationsData}
            onFlyToCoords={onFlyToCoordsHandler}
          />
        )}
      </div>

      <div className={classes.mapContainer}>
        {Object.keys(stationsData).length !== 0 && (
          <Map items={stationsData} flyToItem={flyToItem} />
        )}
      </div>
    </div>
  );
};

export default HomePage;
