import React, { useState, useEffect } from "react";
import { Marker as MapMarker } from "react-map-gl";

import Popup from "../Map/Popup/Popup";
import Pin from "../Pin/Pin";

import { getStationsData } from "../../services/getStationsData";

import classes from "./Marker.module.css";

const Marker = ({ item }) => {
  const [showPopup, setShowpopup] = useState(false);
  const [airQualityLevel, setAirQualityLevel] = useState("");
  const [stationData, setStationData] = useState(undefined);

  const { aqi } = item.properties;

  useEffect(() => {
    setAirQualityLevel(aqi);
  }, [aqi]);

  useEffect(() => {
    const url = `https://api.waqi.info/feed/geo:${item.geometry.coordinates[1]};${item.geometry.coordinates[0]}/?token=${process.env.REACT_APP_AQICN_TOKEN}`;
    getStationsData(url).then(
      (response) => {
        setStationData({
          name: response.city.name,
          aqi: response.aqi,
          time: response.time.s,
          pm25today: response.forecast.daily.pm25[2],
          coordinates: response.city.geo,
        });
      },
      (err) => {
        alert(err);
      }
    );
  }, []);

  const onShowPopupHandler = () => {
    setShowpopup((prevState) => {
      return !prevState;
    });
  };

  return (
    <div
      className={classes.marker}
      onMouseEnter={onShowPopupHandler}
      onMouseLeave={onShowPopupHandler}
    >
      {stationData && (
        <>
          <Popup
            stationData={stationData}
            showPopup={showPopup}
            dynamicPosition={true}
          />
          <MapMarker
            longitude={stationData.coordinates[1]}
            latitude={stationData.coordinates[0]}
          >
            <Pin aqi={airQualityLevel} />
          </MapMarker>{" "}
        </>
      )}
    </div>
  );
};

export default Marker;
