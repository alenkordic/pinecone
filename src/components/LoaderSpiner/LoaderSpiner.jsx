import React from "react";
import Loader from "react-loader-spinner";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";

import classes from "./LoaderSpiner.module.css";

const LoaderSpiner = () => {
  return (
    <div className={classes.loader}>
      <Loader
        type="Circles"
        color="lightslategrey"
        height={100}
        width={100}
        timeout={0}
      />
      <span>Loading...</span>
    </div>
  );
};

export default LoaderSpiner;
