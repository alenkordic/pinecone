import React from "react";
import Switch from "react-switch";
import Logo from "./../Logo/Logo";

import classes from "./SidebarHeader.module.css";

const SidebarHeader = ({ onLoadData, updatedDate, refreshData }) => {
  const onLoadDataHandler = () => {
    // console.log("onLoadData");
    onLoadData();
  };

  //   console.log("updatedTime", updatedTime);

  if (updatedDate) {
  }

  return (
    <div className={classes.contiainer}>
      <div className={classes.header}>
        <Logo className={classes.logo} />
        <div className={classes.switch}>
          <span>Refresh data</span>
          <Switch onChange={onLoadDataHandler} checked={refreshData} />
        </div>
      </div>
      <div className={classes.updated}>
        <p>{`Last update: ${
          updatedDate ? updatedDate.toLocaleString() : "---"
        }`}</p>
      </div>
    </div>
  );
};

export default SidebarHeader;
