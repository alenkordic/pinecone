import React from "react";

import marker0 from "./../../assets/marker-0.svg";
import marker1 from "./../../assets/marker-1.svg";
import marker2 from "./../../assets/marker-2.svg";
import marker3 from "./../../assets/marker-3.svg";
import marker4 from "./../../assets/marker-4.svg";

import { calculateAirHealthLevel } from "./../../helpers/functions";

import classes from "./Pin.module.css";

const Pin = ({ aqi }) => {
  const { index, description } = calculateAirHealthLevel(aqi);
  const imgSrcArr = [marker0, marker1, marker2, marker3, marker4];

  return (
    <div className={classes.pin}>
      <div className={classes.imgContainer}>
        <img src={imgSrcArr[index]} alt={description} />
      </div>
      <div className={classes.description}>{description}</div>
    </div>
  );
};

export default Pin;
