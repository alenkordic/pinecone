import React from 'react'
import StationsListItem from "./StationsListItem/StationsListItem"

import  classes from  "./StationsList.module.css"


const StationsList = ( {items, onFlyToCoords}) => {

    return (
        <ul className={classes.list}>
            {
                items.features.map(el => {
                    return (
                        <StationsListItem key={el.properties.id} item={el.properties} onFlyTo={onFlyToCoords}/>
                    )
                })
            }
        </ul>
    )
}

export default StationsList
