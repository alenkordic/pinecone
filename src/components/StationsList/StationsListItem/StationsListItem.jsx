import React from "react";
import { calculateAirHealthLevel } from "./../../../helpers/functions";
import classes from "./StationsListItem.module.css";

const StationsListItem = ({ item, onFlyTo }) => {
  const { stationName, id, time, aqi } = item;
  const date = new Date(time);

  const onFlyToHadler = () => {
    onFlyTo(id);
  };

  const { description } = calculateAirHealthLevel(aqi);

  return (
    <li className={classes.item}>
      <div>
        <h4 className={classes.name} onClick={onFlyToHadler}>
          {stationName}
        </h4>
      </div>
      <div className={classes.detailsContainer}>
        <div>
          <p className={classes.date}>{date.toLocaleString("en-US")}</p>
          <p className={classes.air}>
            {`Air quality: ${aqi} ppm -`} <span>{description}</span>
          </p>
        </div>
      </div>
    </li>
  );
};

export default StationsListItem;
