import React from 'react'
import pineconeLogo from './../../assets/logo.svg'

import classes from "./Logo.modules.css"

const Logo = ({className}) => {
    return (
        <div className={`${classes.logo} ${className}`}>
            <img src={pineconeLogo} alt="logo" width="250px"/>
        </div>
    )
}

export default Logo
